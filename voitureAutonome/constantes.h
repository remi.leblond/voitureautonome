/* Affectation des connecteurs des moteurs */
#define MOTEUR_IN1 2
#define MOTEUR_IN2 3
#define MOTEUR_IN3 4
#define MOTEUR_IN4 7
#define MOTEUR_FNA 5
#define MOTEUR_FNB 6

/* Configuration du capteur de distance à ultrason */
#define TRIGGER_PIN 10     // Broche TRIGGER - envoi d'impulsion
#define ECHO_PIN 11        // Broche ECHO - réception de l'echo
#define VITESSE_SON 340    // Vitesse du son (m/s)

#define VITESSE_STD 80     // Vitesse standard
#define DIST_POINTE 1600   // Distance permettant une pointe de vitesse (mm)
#define VITESSE_POINTE 120 // Vitesse de pointe
#define DIST_RAPIDE 800    // Distance permettant une vitesse rapide (mm)
#define VITESSE_RAPIDE 100 // Vitesse rapide
#define DIST_ATTENTION 500 // Distance de ralentissement (mm) en vitesse lente
#define VITESSE_LENTE 65
#define DIST_MINI 300      // Distance d'arrêt (mm)

/* Tête rotative du capteur de distance */
#define SERVO_PIN 12        // Broche du servo moteur
#define DEVANT 90           // Regarder devant
#define GAUCHE 135          // Regarder à gauche
#define DROITE 45           // Regarder à droite
#define DELAI_POS_TETE 400  // Delai de positionnement de la tête

/* Vitesse des moteurs lors des rotations */
#define VITESSE_ROTATION 70
#define DELAI_ROTATION 200
 
/* Broche des "feux" stop et de recul */
#define FEU_STOP 8
#define FEU_RECUL 9

/* Broche du capteur de parechoc avant */
#define INT_STOP 13
