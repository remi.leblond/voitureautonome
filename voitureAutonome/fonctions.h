/* Configuration du servomoteur de la tête de mesure ultrason */
#include <Servo.h>
Servo servoTete;

/* Retourne la distance mesurée par le capteur (mm) */
float mesureDistance() 
{
  float distanceMesuree = 0;
  float delaiMesure;
    
  digitalWrite(TRIGGER_PIN, LOW);  // On s'assure que le capteur n'est pas en train d'émettre
  delayMicroseconds(2);
  digitalWrite(TRIGGER_PIN, HIGH); // On emet un signal ultrason (10 ms)
  delayMicroseconds(10);           
  digitalWrite(TRIGGER_PIN, LOW);  // On arrête l'émission

  delaiMesure = pulseIn(ECHO_PIN, HIGH) ; // Mesure du délai de réception du signal (ms)
  distanceMesuree = (delaiMesure-10.0) / 1000.0 * VITESSE_SON / 2.0 ; // Calcul de la distance

  // Serial.println((String)"Delai : " + delaiMesure + " Distance : " + distanceMesuree);
  
  return distanceMesuree;
}

/* Teste si distance de sécurité est respectée */
boolean distanceOK()
{
  if ( mesureDistance() > DIST_MINI ) {
    return true;
  } else {
    return false;
  }
}

/* Retourne la vitesse à laquelle la voiture peut avancer */
int calculVitesse()
{
  int vitesse;
  float distance = mesureDistance();

  if (distance > DIST_POINTE) {
    vitesse = VITESSE_POINTE;
  } else {
    if (distance > DIST_RAPIDE) {
      vitesse = VITESSE_RAPIDE;
    } else {
      if ( distance > DIST_ATTENTION) {
        vitesse = VITESSE_STD;
      } else {
        if (distance < DIST_MINI) {
          vitesse = 0; 
        } else {
          vitesse = VITESSE_LENTE;  
        }
      }  
    }
  }
  
  return vitesse;
}

/*
 Fonction faisant avancer la voiture 
 La vitesse est comprise entre 0 et 256
*/
void avance(int vitesse) {
  Serial.println("Avance");
  analogWrite(MOTEUR_FNA, vitesse * 0.975 ); // Compensation de la différence de puissance entre les deux moteurs
  analogWrite(MOTEUR_FNB, vitesse);

  digitalWrite(MOTEUR_IN1, HIGH);
  digitalWrite(MOTEUR_IN2, LOW);
  digitalWrite(MOTEUR_IN3, HIGH);
  digitalWrite(MOTEUR_IN4, LOW);
  digitalWrite(FEU_RECUL, LOW);
  digitalWrite(FEU_STOP, LOW);
}

/*
 Fonction faisant reculer la voiture 
 La vitesse est comprise entre 0 et 256
*/
void recule(int vitesse) {
  Serial.println("Recule");
  analogWrite(MOTEUR_FNA, vitesse * 0.975 ); // Compensation de la différence de puissance entre les deux moteurs
  analogWrite(MOTEUR_FNB, vitesse);
  digitalWrite(MOTEUR_IN1, LOW);
  digitalWrite(MOTEUR_IN2, HIGH);
  digitalWrite(MOTEUR_IN3, LOW);
  digitalWrite(MOTEUR_IN4, HIGH);
  digitalWrite(FEU_RECUL, HIGH);
  digitalWrite(FEU_STOP, LOW);
}

/* Arrêt des moteurs */
void stop() {
  Serial.println("Arrêt des moteurs");
  analogWrite(MOTEUR_FNA, 0);
  analogWrite(MOTEUR_FNB, 0);
  digitalWrite(MOTEUR_IN1, LOW);
  digitalWrite(MOTEUR_IN2, LOW);
  digitalWrite(MOTEUR_IN3, LOW);
  digitalWrite(MOTEUR_IN4, LOW);
  digitalWrite(FEU_RECUL, LOW);
  digitalWrite(FEU_STOP, HIGH);
}

/* Détection d'un touché sur le parechoc avant */
boolean touche () {
  if (digitalRead (INT_STOP) == HIGH ) {
    return true;
  }
  else {
    return false;
  }
}

/* Rotation vers la gauche */
void tourneGauche() {
  Serial.println("Tourne à gauche");
  digitalWrite(FEU_STOP, LOW);
  analogWrite(MOTEUR_FNA, VITESSE_ROTATION);
  analogWrite(MOTEUR_FNB, VITESSE_ROTATION);
  digitalWrite(MOTEUR_IN1, HIGH);
  digitalWrite(MOTEUR_IN2, LOW);
  digitalWrite(MOTEUR_IN3, LOW);
  digitalWrite(MOTEUR_IN4, HIGH);
  digitalWrite(FEU_RECUL, LOW);
  delay(DELAI_ROTATION);
  stop();
}

/* Rotation vers la droite */
void tourneDroite() {
  Serial.println("Tourne à gauche");
  digitalWrite(FEU_STOP, LOW);
  analogWrite(MOTEUR_FNA, VITESSE_ROTATION);
  analogWrite(MOTEUR_FNB, VITESSE_ROTATION);
  digitalWrite(MOTEUR_IN1, LOW);
  digitalWrite(MOTEUR_IN2, HIGH);
  digitalWrite(MOTEUR_IN3, HIGH);
  digitalWrite(MOTEUR_IN4, LOW);
  digitalWrite(FEU_RECUL, LOW);
  delay(DELAI_ROTATION);
  stop();
}

/* Demi-tour complet */
void demiTour() {
  Serial.println("Demi tour");
  digitalWrite(FEU_RECUL, HIGH);
  digitalWrite(FEU_STOP, LOW);
  analogWrite(MOTEUR_FNA, VITESSE_ROTATION);
  analogWrite(MOTEUR_FNB, VITESSE_ROTATION);
  digitalWrite(MOTEUR_IN1, LOW);
  digitalWrite(MOTEUR_IN2, HIGH);
  digitalWrite(MOTEUR_IN3, HIGH);
  digitalWrite(MOTEUR_IN4, LOW);
  delay(DELAI_ROTATION*1.5);
  stop();
}

/* Rotation de la tête de mesure de distance */
void regarde(int angle) {
  servoTete.write(angle);
  delay(DELAI_POS_TETE);
}
 
/* Positionnement avant de la tête de mesure de distance */
void regardeDevant() {
  regarde(DEVANT);
}

/* Positionnement à droite de la tête de mesure de distance */
void regardeDroite() {
  regarde(DROITE);
}

/* Positionnement à gauche de la tête de mesure de distance */
void regardeGauche() {
  regarde(GAUCHE);
}

/* Clignotement des feux stop */
void clignote () {
  int i;
  for (i=0; i<10; i++) {
    digitalWrite(FEU_STOP, HIGH);
    delay(200);
    digitalWrite(FEU_STOP, LOW);
    delay(500 - (50 * i));
  }
}
