/* Configuration du récepteur :
Type de carte : Arduino/Genuino Uno
Programmeur : AVR ISP
*/

#include "constantes.h"
#include "fonctions.h"

int vitesse; // Vitesse du moteur

/* Initialisation du système */
void setup() {
  Serial.begin(9600);

  /* Attachement du servo moteur de la tête du capteur */
  servoTete.attach(SERVO_PIN);
  Serial.print("Test des mouvements de la tête... ");
  regardeGauche();
  regardeDroite();
  regardeDevant();
  Serial.println("OK");

  /* Initialisation du capteur de distance */
  pinMode(TRIGGER_PIN, OUTPUT);
  digitalWrite(TRIGGER_PIN, LOW); // La broche TRIGGER doit être à LOW au repos
  pinMode(ECHO_PIN, INPUT);

  /* Initialisation du capteur de parechoc */
  pinMode(INT_STOP, INPUT);

  /* Initialisation des moteurs */
  pinMode(MOTEUR_IN1, OUTPUT);
  pinMode(MOTEUR_IN2, OUTPUT);
  pinMode(MOTEUR_IN3, OUTPUT);
  pinMode(MOTEUR_IN4, OUTPUT);
  pinMode(MOTEUR_FNA, OUTPUT);
  pinMode(MOTEUR_FNB, OUTPUT);
  analogWrite(MOTEUR_FNA, 100); // Vitesse par défaut
  analogWrite(MOTEUR_FNB, 100);

  /* Initialisation des "feux" stop et de recul */
  pinMode(FEU_STOP, OUTPUT);
  digitalWrite(FEU_STOP, LOW);
  pinMode(FEU_RECUL, OUTPUT);
  digitalWrite(FEU_RECUL, LOW);

  stop();

  /* Clignotement des feux */
  clignote();
}

/* Programme principal */
void loop() {
  if (touche()) {
    stop();
    recule(VITESSE_LENTE);
    delay(300);
    demiTour();
  } else {
    vitesse = calculVitesse();
    if (vitesse > 0) {
      avance(vitesse);
    } else {
      stop();
      regardeGauche();
      if (distanceOK()) {
        tourneGauche();
        regardeDevant();
        avance(vitesse);
      } else {
        regardeDroite();
        if (distanceOK()) {
          tourneDroite();
          regardeDevant();
          avance(vitesse);
        } else {
          recule(VITESSE_LENTE);
          delay(300);
          demiTour();
          stop();
          regardeDevant();
        }
      }
    }
  }
}
